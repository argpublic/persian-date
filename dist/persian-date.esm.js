/**
 * Parse or format dates
 * @class fecha
 */
var twoDigits = /\d\d?/;
var threeDigits = /\d{3}/;
var fourDigits = /\d{4}/;
var word = /[^\s]+/i;
var noop = function noop() {};

function shorten(arr, sLen) {
  var newArr = [];
  for (var i = 0, len = arr.length; i < len; i++) {
    newArr.push(arr[i].substr(0, sLen));
  }
  return newArr;
}

function monthUpdate(arrName) {
  return function (d, v, i18n) {
    var index = i18n[arrName].indexOf(v.charAt(0).toUpperCase() + v.substr(1).toLowerCase());
    if (~index) {
      d.month = index;
    }
  };
}

function pad(val, len) {
  val = String(val);
  len = len || 2;
  while (val.length < len) {
    val = '0' + val;
  }
  return val;
}

function enToFaNumber(val) {
  if (typeof val === 'undefined') {
    return '';
  }

  var value = val;

  value = '' + value;

  value = value.replace(/0/g, '۰');
  value = value.replace(/1/g, '۱');
  value = value.replace(/2/g, '۲');
  value = value.replace(/3/g, '۳');
  value = value.replace(/4/g, '۴');
  value = value.replace(/5/g, '۵');
  value = value.replace(/6/g, '۶');
  value = value.replace(/7/g, '۷');
  value = value.replace(/8/g, '۸');
  value = value.replace(/9/g, '۹');

  return value;
}

function faToEnNumber(val) {
  var enValue = '';

  for (var i = 0; i < val.length; i++) {
    var ch = val.charCodeAt(i);

    if (ch >= 1776 && ch <= 1785) // For Persian digits.
      {
        var newChar = ch - 1728;

        enValue += String.fromCharCode(newChar);
      } else if (ch >= 1632 && ch <= 1641) // For Arabic & Unix digits.
      {
        var _newChar = ch - 1584;

        enValue += String.fromCharCode(_newChar);
      } else {
      enValue += String.fromCharCode(ch);
    }
  }

  return enValue;
}

var masks = {
    default: 'ddd MMM DD YYYY HH:mm:ss',
    shortDate: 'M/D/YY',
    mediumDate: 'MMM D, YYYY',
    longDate: 'MMMM D, YYYY',
    fullDate: 'dddd, MMMM D, YYYY',
    shortTime: 'HH:mm',
    mediumTime: 'HH:mm:ss',
    longTime: 'HH:mm:ss.SSS'
};

var token = /d{1,4}|M{1,4}|YY(?:YY)?|S{1,3}|Do|ZZ|([HhMsDm])\1?|[aA]|"[^"]*"|'[^']*'/g;
var literal = /\[([^]*?)\]/gm;

var formatFlags = {
    D: function D(dateObj, i18n) {
        return dateObj['get' + i18n.funcName + 'Date']();
    },
    DD: function DD(dateObj, i18n) {
        return pad(dateObj['get' + i18n.funcName + 'Date']());
    },
    Do: function Do(dateObj, i18n) {
        return i18n.DoFn(dateObj['get' + i18n.funcName + 'Date']());
    },
    d: function d(dateObj) {
        return dateObj.getDay();
    },
    dd: function dd(dateObj) {
        return pad(dateObj.getDay());
    },
    ddd: function ddd(dateObj, i18n) {
        return i18n.dayNamesShort[dateObj.getDay()];
    },
    dddd: function dddd(dateObj, i18n) {
        return i18n.dayNames[dateObj.getDay()];
    },
    M: function M(dateObj, i18n) {
        return dateObj['get' + i18n.funcName + 'Month']() + 1;
    },
    MM: function MM(dateObj, i18n) {
        return pad(dateObj['get' + i18n.funcName + 'Month']() + 1);
    },
    MMM: function MMM(dateObj, i18n) {
        return i18n.monthNamesShort[dateObj['get' + i18n.funcName + 'Month']()];
    },
    MMMM: function MMMM(dateObj, i18n) {
        return i18n.monthNames[dateObj['get' + i18n.funcName + 'Month']()];
    },
    YY: function YY(dateObj, i18n) {
        return String(dateObj['get' + i18n.funcName + 'FullYear']()).substr(2);
    },
    YYYY: function YYYY(dateObj, i18n) {
        return pad(dateObj['get' + i18n.funcName + 'FullYear'](), 4);
    },
    h: function h(dateObj) {
        return dateObj.getHours() % 12 || 12;
    },
    hh: function hh(dateObj) {
        return pad(dateObj.getHours() % 12 || 12);
    },
    H: function H(dateObj) {
        return dateObj.getHours();
    },
    HH: function HH(dateObj) {
        return pad(dateObj.getHours());
    },
    m: function m(dateObj) {
        return dateObj.getMinutes();
    },
    mm: function mm(dateObj) {
        return pad(dateObj.getMinutes());
    },
    s: function s(dateObj) {
        return dateObj.getSeconds();
    },
    ss: function ss(dateObj) {
        return pad(dateObj.getSeconds());
    },
    S: function S(dateObj) {
        return Math.round(dateObj.milliseconds() / 100);
    },
    SS: function SS(dateObj) {
        return pad(Math.round(dateObj.getMilliseconds() / 10), 2);
    },
    SSS: function SSS(dateObj) {
        return pad(dateObj.getMilliseconds(), 3);
    },
    a: function a(dateObj, i18n) {
        return dateObj.getHours() < 12 ? i18n.amPm[0] : i18n.amPm[1];
    },
    A: function A(dateObj, i18n) {
        return dateObj.getHours() < 12 ? i18n.amPm[0].toUpperCase() : i18n.amPm[1].toUpperCase();
    },
    ZZ: function ZZ(dateObj) {
        var o = dateObj.getTimezoneOffset();
        return (o > 0 ? '-' : '+') + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4);
    }
};

var parseFlags = {
    D: [twoDigits, function (d, v) {
        d.day = parseInt(v);
    }],
    Do: [new RegExp(twoDigits.source + word.source), function (d, v) {
        d.day = parseInt(v, 10);
    }],
    M: [twoDigits, function (d, v) {
        d.month = parseInt(v - 1);
    }],
    YY: [twoDigits, function (d, v, i18n) {
        var da = new Date();var cent = +('' + da['get' + i18n.funcName + 'FullYear']()).substr(0, 2);
        d.year = '' + (v > 68 ? cent - 1 : cent) + v;
    }],
    h: [twoDigits, function (d, v) {
        d.hour = parseInt(v);
    }],
    m: [twoDigits, function (d, v) {
        d.minute = parseInt(v);
    }],
    s: [twoDigits, function (d, v) {
        d.second = parseInt(v);
    }],
    YYYY: [fourDigits, function (d, v) {
        d.year = parseInt(v);
    }],
    S: [/\d/, function (d, v) {
        d.millisecond = v * 100;
    }],
    SS: [/\d{2}/, function (d, v) {
        d.millisecond = v * 10;
    }],
    SSS: [threeDigits, function (d, v) {
        d.millisecond = parseInt(v);
    }],
    d: [twoDigits, noop],
    ddd: [word, noop],
    MMM: [word, monthUpdate('monthNamesShort')],
    MMMM: [word, monthUpdate('monthNames')],
    a: [word, function (d, v, i18n) {
        var val = v.toLowerCase();
        if (val === i18n.amPm[0]) {
            d.isPm = false;
        } else if (val === i18n.amPm[1]) {
            d.isPm = true;
        }
    }],
    ZZ: [/([\+\-]\d\d:?\d\d|Z)/, function (d, v) {
        if (v === 'Z') v = '+00:00';
        var parts = (v + '').match(/([\+\-]|\d\d)/gi);var minutes;

        if (parts) {
            minutes = +(parts[1] * 60) + parseInt(parts[2], 10);
            d.timezoneOffset = parts[0] === '+' ? minutes : -minutes;
        }
    }]
};
parseFlags.dd = parseFlags.d;
parseFlags.dddd = parseFlags.ddd;
parseFlags.DD = parseFlags.D;
parseFlags.mm = parseFlags.m;
parseFlags.hh = parseFlags.H = parseFlags.HH = parseFlags.h;
parseFlags.MM = parseFlags.M;
parseFlags.ss = parseFlags.s;
parseFlags.A = parseFlags.a;

/***
 * Format a date
 * @method format
 * @param {Date|number} dateObj
 * @param i18n
 * @param {string} mask Format of the date, i.e. 'mm-dd-yy' or 'shortDate'
 */
function format(dateObj, i18n, mask) {
    if (typeof dateObj === 'number') {
        dateObj = new Date(dateObj);
    }

    if (Object.prototype.toString.call(dateObj) !== '[object Date]' || isNaN(dateObj.getTime())) {
        throw new Error('Invalid Date in fecha.format');
    }

    mask = masks[mask] || mask || masks['default'];

    var literals = [];

    // Make literals inactive by replacing them with ??
    mask = mask.replace(literal, function ($0, $1) {
        literals.push($1);
        return '??';
    });
    // Apply formatting rules
    mask = mask.replace(token, function ($0) {
        return $0 in formatFlags ? formatFlags[$0](dateObj, i18n) : $0.slice(1, $0.length - 1);
    });
    // Inline literal values back into the formatted value
    return mask.replace(/\?\?/g, function () {
        return literals.shift();
    });
}

/**
 * Parse a date string into an object, changes - into /
 * @method parse
 * @param {string} dateStr Date string
 * @param {string} format Date parse format
 * @param i18n
 * @returns {Date|boolean}
 */
function parse(dateStr, format, i18n) {
    if (typeof format !== 'string') {
        throw new Error('Invalid format in fecha.parse');
    }

    format = masks[format] || format;

    // Avoid regular expression denial of service, fail early for really long strings
    // https://www.owasp.org/index.php/Regular_expression_Denial_of_Service_-_ReDoS
    if (dateStr.length > 1000) {
        return false;
    }

    var isValid = true;
    var dateInfo = {};
    format.replace(token, function ($0) {
        if (parseFlags[$0]) {
            var info = parseFlags[$0];
            var index = dateStr.search(info[0]);
            if (!~index) {
                isValid = false;
            } else {
                dateStr.replace(info[0], function (result) {
                    info[1](dateInfo, result, i18n);
                    dateStr = dateStr.substr(index + result.length);
                    return result;
                });
            }
        }

        return parseFlags[$0] ? '' : $0.slice(1, $0.length - 1);
    });

    if (!isValid) {
        return false;
    }

    var today = new Date();
    if (dateInfo.isPm === true && dateInfo.hour != null && +dateInfo.hour !== 12) {
        dateInfo.hour = +dateInfo.hour + 12;
    } else if (dateInfo.isPm === false && +dateInfo.hour === 12) {
        dateInfo.hour = 0;
    }

    var date;
    if (dateInfo.timezoneOffset != null) {
        dateInfo.minute = +(dateInfo.minute || 0) - +dateInfo.timezoneOffset;
        date = new Date()['set' + i18n.funcName + 'DateTime'](Date[i18n.funcName + 'UTC'](dateInfo.year || today.year(), dateInfo.month || 0, dateInfo.day || 1, dateInfo.hour || 0, dateInfo.minute || 0, dateInfo.second || 0, dateInfo.millisecond || 0));
    } else {
        date = new Date()['set' + i18n.funcName + 'DateTime'](dateInfo.year || today.getJalaliFullYear(), dateInfo.month || 0, dateInfo.day || 1, dateInfo.hour || 0, dateInfo.minute || 0, dateInfo.second || 0, dateInfo.millisecond || 0);
    }
    return date;
}

var dayNames = ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'];
var monthNamesArr = {
  'fa': ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
  'en': ['farvardin', 'ordibehesht', 'khordad', 'tir', 'mordad', 'shahrivar', 'mehr', 'aban', 'azar', 'dey', 'bahman', 'esfand']
};

function i18nfa () {
  var locale = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'fa';


  var monthNames = monthNamesArr[locale];
  var monthNamesShort = shorten(monthNames, 3);
  var dayNamesShort = shorten(dayNames, 3);

  return {
    locale: locale,
    dayNamesShort: dayNamesShort,
    dayNames: dayNames,
    monthNamesShort: monthNamesShort,
    monthNames: monthNames,
    amPm: ['ق.ظ', 'ب.ظ'],
    DoFn: function DoFn(D) {
      return D + ['th', 'st', 'nd', 'rd'][D % 10 > 3 ? 0 : (D - D % 10 !== 10) * D % 10];
    },
    funcName: 'Jalali'
  };
}

var dayNames$1 = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var monthNamesArr$1 = {
  fa: ['زانویه', 'فوریه', 'مارس', 'آوریل', 'می', 'ژوئن', 'جولای', 'آگوست', 'سپتامبر', 'اکتبر', 'نوامبر', 'دسامبر'],
  en: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
};

function i18nen () {
  var locale = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'en';


  var monthNames = monthNamesArr$1[locale];
  var monthNamesShort = shorten(monthNames, 3);
  var dayNamesShort = shorten(dayNames$1, 3);

  return {
    dayNamesShort: dayNamesShort,
    dayNames: dayNames$1,
    monthNamesShort: monthNamesShort,
    monthNames: monthNames,
    amPm: ['am', 'pm'],
    DoFn: function DoFn(D) {
      return D + ['th', 'st', 'nd', 'rd'][D % 10 > 3 ? 0 : (D - D % 10 !== 10) * D % 10];
    },
    funcName: ''
  };
}

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var i18ns = {
  'en': i18nen,
  'fa': i18nfa
};

function getI18n(i18n, locale) {

  if ((typeof i18n === "undefined" ? "undefined" : _typeof(i18n)) === "object") {
    if (i18n.name) i18n = _extends({}, i18ns[i18n](locale), i18n);
    return i18n;
  }

  return i18ns[i18n](locale);
}

/*
 * JalaliJSCalendar - Jalali Extension for Date Object
 * Copyright (c) 2008 Ali Farhadi (http://farhadi.ir/)
 * Released under the terms of the GNU General Public License.
 * See the GPL for details (http://www.gnu.org/licenses/gpl.html).
 *
 * Based on code from http://farsiweb.info
 */

var JalaliDate = {
  g_days_in_month: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
  j_days_in_month: [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]
};

JalaliDate.jalaliToGregorian = function (j_y, j_m, j_d) {
  j_y = parseInt(j_y);
  j_m = parseInt(j_m);
  j_d = parseInt(j_d);
  var jy = j_y - 979;
  var jm = j_m - 1;
  var jd = j_d - 1;

  var j_day_no = 365 * jy + parseInt(jy / 33) * 8 + parseInt((jy % 33 + 3) / 4);
  for (var i = 0; i < jm; ++i) {
    j_day_no += JalaliDate.j_days_in_month[i];
  }j_day_no += jd;

  var g_day_no = j_day_no + 79;

  var gy = 1600 + 400 * parseInt(g_day_no / 146097); /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
  g_day_no = g_day_no % 146097;

  var leap = true;
  if (g_day_no >= 36525) /* 36525 = 365*100 + 100/4 */
    {
      g_day_no--;
      gy += 100 * parseInt(g_day_no / 36524); /* 36524 = 365*100 + 100/4 - 100/100 */
      g_day_no = g_day_no % 36524;

      if (g_day_no >= 365) {
        g_day_no++;
      } else {
        leap = false;
      }
    }

  gy += 4 * parseInt(g_day_no / 1461); /* 1461 = 365*4 + 4/4 */
  g_day_no %= 1461;

  if (g_day_no >= 366) {
    leap = false;

    g_day_no--;
    gy += parseInt(g_day_no / 365);
    g_day_no = g_day_no % 365;
  }

  for (var i = 0; g_day_no >= JalaliDate.g_days_in_month[i] + (i == 1 && leap); i++) {
    g_day_no -= JalaliDate.g_days_in_month[i] + (i == 1 && leap);
  }
  var gm = i + 1;
  var gd = g_day_no + 1;

  return [gy, gm, gd];
};

JalaliDate.checkDate = function (j_y, j_m, j_d) {
  return !(j_y < 0 || j_y > 32767 || j_m < 1 || j_m > 12 || j_d < 1 || j_d > JalaliDate.j_days_in_month[j_m - 1] + (j_m == 12 && !((j_y - 979) % 33 % 4)));
};

JalaliDate.gregorianToJalali = function (g_y, g_m, g_d) {
  g_y = parseInt(g_y);
  g_m = parseInt(g_m);
  g_d = parseInt(g_d);
  var gy = g_y - 1600;
  var gm = g_m - 1;
  var gd = g_d - 1;

  var g_day_no = 365 * gy + parseInt((gy + 3) / 4) - parseInt((gy + 99) / 100) + parseInt((gy + 399) / 400);

  for (var i = 0; i < gm; ++i) {
    g_day_no += JalaliDate.g_days_in_month[i];
  }
  if (gm > 1 && (gy % 4 == 0 && gy % 100 != 0 || gy % 400 == 0))
    /* leap and after Feb */
    {
      ++g_day_no;
    }
  g_day_no += gd;

  var j_day_no = g_day_no - 79;

  var j_np = parseInt(j_day_no / 12053);
  j_day_no %= 12053;

  var jy = 979 + 33 * j_np + 4 * parseInt(j_day_no / 1461);

  j_day_no %= 1461;

  if (j_day_no >= 366) {
    jy += parseInt((j_day_no - 1) / 365);
    j_day_no = (j_day_no - 1) % 365;
  }

  for (var i = 0; i < 11 && j_day_no >= JalaliDate.j_days_in_month[i]; ++i) {
    j_day_no -= JalaliDate.j_days_in_month[i];
  }
  var jm = i + 1;
  var jd = j_day_no + 1;

  return [jy, jm, jd];
};

JalaliDate.lastDayOfMonth = function (y, m) {
  if (m >= 1 && m <= 6) {
    return 31;
  } else if (m >= 7 && m < 12) {
    return 30;
  } else if (m !== 12) {
    throw new Error("Invalid date");
  }
  /* Esfand */
  if (((y - (y > 0 ? 474 : 473)) % 2820 + 474 + 38) * 682 % 2816 < 682) {
    /* Leap year */
    return 30;
  }
  return 29;
};

Date.prototype.setJalaliFullYear = function (y, m, d) {
  var gd = this.getDate();
  var gm = this.getMonth();
  var gy = this.getFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  if (y < 100) y += 1300;
  j[0] = y;
  if (m != undefined) {
    if (m > 11) {
      j[0] += Math.floor(m / 12);
      m = m % 12;
    }
    j[1] = m + 1;
  }
  if (d != undefined) j[2] = d;
  var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);
  return this.setFullYear(g[0], g[1] - 1, g[2]);
};

Date.prototype.setJalaliMonth = function (m, d) {
  var gd = this.getDate();
  var gm = this.getMonth();
  var gy = this.getFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  if (m > 11) {
    j[0] += Math.floor(m / 12);
    m = m % 12;
  }
  j[1] = m + 1;
  if (d != undefined) j[2] = d;
  var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);
  return this.setFullYear(g[0], g[1] - 1, g[2]);
};

Date.prototype.setJalaliDate = function (d) {
  var gd = this.getDate();
  var gm = this.getMonth();
  var gy = this.getFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  j[2] = d;
  var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);
  return this.setFullYear(g[0], g[1] - 1, g[2]);
};

Date.prototype.getJalaliFullYear = function () {
  var gd = this.getDate();
  var gm = this.getMonth();
  var gy = this.getFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  return j[0];
};

Date.prototype.getJalaliMonth = function () {
  var gd = this.getDate();
  var gm = this.getMonth();
  var gy = this.getFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  return j[1] - 1;
};

Date.prototype.getJalaliDate = function () {
  var gd = this.getDate();
  var gm = this.getMonth();
  var gy = this.getFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  return j[2];
};

Date.prototype.getJalaliDay = function () {
  var day = this.getDay();
  day = (day + 1) % 7;
  return day;
};

/**
 * Jalali UTC functions
 */

Date.prototype.setJalaliUTCFullYear = function (y, m, d) {
  var gd = this.getUTCDate();
  var gm = this.getUTCMonth();
  var gy = this.getUTCFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  if (y < 100) y += 1300;
  j[0] = y;
  if (m != undefined) {
    if (m > 11) {
      j[0] += Math.floor(m / 12);
      m = m % 12;
    }
    j[1] = m + 1;
  }
  if (d != undefined) j[2] = d;
  var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);
  return this.setUTCFullYear(g[0], g[1] - 1, g[2]);
};

Date.prototype.setJalaliUTCMonth = function (m, d) {
  var gd = this.getUTCDate();
  var gm = this.getUTCMonth();
  var gy = this.getUTCFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  if (m > 11) {
    j[0] += Math.floor(m / 12);
    m = m % 12;
  }
  j[1] = m + 1;
  if (d != undefined) j[2] = d;
  var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);
  return this.setUTCFullYear(g[0], g[1] - 1, g[2]);
};

Date.prototype.setJalaliUTCDate = function (d) {
  var gd = this.getUTCDate();
  var gm = this.getUTCMonth();
  var gy = this.getUTCFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  j[2] = d;
  var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);
  return this.setUTCFullYear(g[0], g[1] - 1, g[2]);
};

Date.prototype.getJalaliUTCFullYear = function () {
  var gd = this.getUTCDate();
  var gm = this.getUTCMonth();
  var gy = this.getUTCFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  return j[0];
};

Date.prototype.getJalaliUTCMonth = function () {
  var gd = this.getUTCDate();
  var gm = this.getUTCMonth();
  var gy = this.getUTCFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  return j[1] - 1;
};

Date.prototype.getJalaliUTCDate = function () {
  var gd = this.getUTCDate();
  var gm = this.getUTCMonth();
  var gy = this.getUTCFullYear();
  var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);
  return j[2];
};

Date.prototype.getJalaliUTCDay = function () {
  var day = this.getUTCDay();
  day = (day + 1) % 7;
  return day;
};

Date.jalaliUTC = function (y, m, d, h, min, sec, mil) {
  var date = Date.getJalaliDateTime(y, m, d, h, min, sec, mil);
  return Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
};

Date.prototype.setJalaliDateTime = function () {
  this.setTime(Date.getJalaliDateTime.apply(Date, arguments).getTime());
  return this;
};

Date.getJalaliDateTime = function () {
  for (var _len = arguments.length, date = Array(_len), _key = 0; _key < _len; _key++) {
    date[_key] = arguments[_key];
  }

  if (date.length === 0) {
    date.push(new Date());
  }

  if (date.length === 1 && (typeof date[0] === 'number' || date[0] instanceof Date)) {
    return new Date(date);
  }

  if (typeof date[0] === 'string') {
    date[1] = date[1] || 'YYYY-MM-D HH:mm:ss';
    return new Date(Date.parseJalaliDate(date[0], date[1]).getTime());
  }

  var dateTime = new Date();

  dateTime.setJalaliFullYear.apply(dateTime, _toConsumableArray(date.slice(0, 3)));

  var time = date.slice(3);

  if (time.length > 0) dateTime.setHours.apply(dateTime, _toConsumableArray(time));

  return dateTime;
};

Date.prototype.setDateTime = function () {
  this.setTime(Date.getDateTime.apply(Date, arguments));
  return this;
};

Date.getDateTime = function () {
  for (var _len2 = arguments.length, date = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    date[_key2] = arguments[_key2];
  }

  if (typeof date[0] === 'string') {
    date[1] = date[1] || 'YYYY-MM-D HH:mm:ss';
    return new Date(Date.parseDate(date[0], date[1]).getTime());
  }

  return new (Function.prototype.bind.apply(Date, [null].concat(date)))();
};

Date.prototype.formatDate = function (mask) {
  var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'en';
  var i18n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'en';

  return Date.formatDate(this, mask, i18n, locale);
};

Date.formatDate = function (date, mask) {
  var i18n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'en';
  var locale = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'en';

  try {
    return format(date, getI18n(i18n, locale), mask);
  } catch (e) {
    return '';
  }
};

Date.parseDate = function (dateStr, format$$1) {
  var i18n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'en';
  var locale = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'en';

  try {
    return parse(dateStr, format$$1, getI18n(i18n, locale));
  } catch (e) {
    return null;
  }
};

Date.prototype.formatJalaliDate = function (mask, locale) {
  var i18n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'fa';

  return Date.formatJalaliDate(this, mask, locale, i18n);
};

Date.formatJalaliDate = function (date, mask, locale) {
  var i18n = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'fa';

  var dateStr = Date.formatDate(date, mask, i18n, locale);
  return locale === 'fa' ? enToFaNumber(dateStr) : dateStr;
};

Date.parseJalaliDate = function (dateStr, format$$1, locale) {
  var i18n = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'fa';

  if (locale === 'fa') {
    dateStr = faToEnNumber(dateStr);
  }
  return Date.parseDate(dateStr, format$$1, i18n, locale);
};

Date.prototype.isLeap = function () {
  var year = this.getFullYear();
  return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
};

Date.prototype.isJalaliLeap = function () {
  return JalaliDate.lastDayOfMonth(this.getJalaliFullYear(), 12) === 30;
};

Date.prototype.daysInMonth = function (m, y) {
  return new Date(y || this.getFullYear(), m || this.getMonth(), 0).getDate();
};

Date.prototype.daysInJalaliMonth = function (m, y) {
  return JalaliDate.lastDayOfMonth(y || this.getJalaliFullYear(), m || this.getJalaliMonth());
};

Date.prototype.addMonths = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  this.setMonth(this.getMonth() + m);
  return this;
};

Date.prototype.subMonths = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : -1;

  return this.addMonths(-m);
};

Date.prototype.addDays = function () {
  var d = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  this.setTime(this.getTime() + d * 86400000);
  return this;
};

Date.prototype.subDays = function () {
  var d = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  return this.addDays(-d);
};

Date.prototype.addHours = function () {
  var h = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  this.setTime(this.getTime() + h * 3600000);
  return this;
};

Date.prototype.subHours = function () {
  var h = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  return this.addHours(-h);
};

Date.prototype.addMinutes = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  this.setTime(this.getTime() + m * 60000);
  return this;
};

Date.prototype.subMinutes = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  return this.addMinutes(-m);
};

Date.prototype.addSeconds = function () {
  var s = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  this.setTime(this.getTime() + s * 1000);
  return this;
};

Date.prototype.subSeconds = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  return this.addSeconds(-m);
};

Date.prototype.addMilliseconds = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  this.setTime(this.getTime() + m);
  return this;
};

Date.prototype.subMilliseconds = function () {
  var m = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  return this.addMilliseconds(-m);
};
